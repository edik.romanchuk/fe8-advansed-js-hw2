const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];



const div = document.createElement('div');
div.setAttribute('id', 'root');
document.body.appendChild(div);
const ul = document.createElement('ul');
document.getElementById('root').appendChild(ul);


books.forEach ((element) => {

  try {
    if (!element.author){
      throw new SyntaxError ('No author')
    }else if(!element.name){     
      throw new SyntaxError ('No name')
    } else if (!element.price) {
      throw new SyntaxError ('No price')
    } else {
      let listItem = document.createElement('li');
      listItem.innerHTML = JSON.stringify(element, null, '  ')
      ul.append(listItem);
    }    
  }
  
  catch(err) {
      console.log(err)
  }
})

